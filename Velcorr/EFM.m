clc
close all

t = 0:0.001:1;

% x1 = cos(2*pi*2*t)*1023;
% x2 = cos(2*pi*5*t)*1023;
%plot(t, x1+x2)
x1 = cos(2*pi*2*t);
x2 = cos(2*pi*5*t);
xt=((x1+x2))+2;
xt=xt/max(xt);
h=ceil(xt*(4095-241))+241;
plot(t,h)
min(h)
max(h)
%stem(t, ceil((x1+x2)*0.5))
%stem(t, ceil((x1+x2)+2047))
%xlswrite (EFM, A)
A = [ t; h];
b = dec2hex(A(2,:))
baux={};
for i=1:length(b)
baux{i}=['0x' b(i,:) ','];
end
char(baux)
plot(A(1,:),A(2,:))
dlmwrite('spillData.txt',baux,'delimiter','');
% csvwrite('EFMdatos.txt',baux)

title('T�cnica EFM')
xlabel('t (s)')
ylabel('E-Ecorr (mV)')