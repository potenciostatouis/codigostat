function [s, harmonics, freq, i_fft, CF, ctrnt] = efm_fft(t, E, I, varargin)
    
    % Default Values
    %---------------------------------------------------------
    freq1 = 0.2;
    freq2 = 0.5;

    Cdl   = 0;
    Fs    = 0;
    
    Cdl_flag = 1;
    Cm_flag  = 0;
    
    % Argument parse
    %---------------------------------------------------------
    if nargin-3>0
        for k=1:nargin-3
            if isnumeric(varargin{k}) && length(varargin{k})==2
                freqs = varargin{k};
            elseif strcmp(varargin{k}, 'Cdl')
                Cdl = varargin{k+1};
            elseif strcmp(varargin{k}, 'Fs')
                Fs = varargin{k+1};
            elseif strcmp(varargin{k}, 'Cdl_On')
                Cdl_flag = 1;
            elseif strcmp(varargin{k}, 'Cdl_Off')
                Cdl_flag = 0;
            elseif strcmp(varargin{k}, 'Cm_On')
                Cm_flag = 1;
            elseif strcmp(varargin{k}, 'Cm_Off')
                Cm_flag = 0;
            end
        end
    end


    % FFT
    %---------------------------------------------------------
    Ecorr = mean(E);
    A = max(abs(E-Ecorr))/2;
        
    if Fs==0
        T = mean(diff(t));
        Fs = 1/T;
    end
    
    L = length(I);
    P2 = abs(fft(I)/L);         
    P1 = P2(1:floor(L/2)+1);
    P1(2:end-1) = 2*P1(2:end-1);
    i_fft = P1;                      % Transformada discreta de Fourier 
    freq = Fs*(0:floor(L/2))/L;    % Eje de frecuencias en Hz
    
    % Calculo de las corrientes de intermodulacion
    %--------------------------------------------------
    epsilon = min(freqs)/10; % se define un error
    %epsilon = 0.1; % se define un error
    freq1 = min(freqs);
    freq2 = max(freqs);

    f_w2m2w1   = freq2-2*freq1;
    f_w1       = freq1;
    f_4w1mw2   = 4*freq1-freq2;
    f_2w1      = 2*freq1;
    f_w2       = freq2;
    f_3w1      = 3*freq1;
    f_w1pw2    = freq1+freq2;
    f_2w2mw1   = 2*freq2-freq1;
    f_4w1      = 4*freq1;
    f_2w1pw2   = 2*freq1+freq2;
    f_2w2      = 2*freq2;
    f_5w1      = 5*freq1;
    f_3w1pw2   = 3*freq1+freq2;
    f_3w2m2w1  = 3*freq2-2*freq1;
    f_w1p2w2   = freq1+2*freq2;
    f_4w1pw2   = 4*freq1+freq2;
    f_3w2mw1   = 3*freq2-freq1;
    f_2w1p2w2  = 2*freq1+2*freq2;
    f_3w2      = 3*freq2;
    f_3w1p2w2  = 3*freq1+2*freq2;
    f_w1p3w2   = freq1+3*freq2;
    f_2w1p3w2  = 2*freq1+3*freq2;
    f_4w2      = 4*freq2;
    f_w1p4w2   = freq1+4*freq2;
    f_5w2      = 5*freq2;


    %[i_peaks, idx] = findpeaks(i_fft);
    %freq_peaks = freq(idx);
    
    i_peaks = i_fft;
    freq_peaks = freq;

    i_w2m2w1   = max(abs(i_peaks((freq_peaks>abs(f_w2m2w1-epsilon))&(freq_peaks<f_w2m2w1+epsilon))));
    i_w1       = max(abs(i_peaks((freq_peaks>abs(f_w1-epsilon))&(freq_peaks<f_w1+epsilon))));
    i_4w1mw2   = max(abs(i_peaks((freq_peaks>abs(f_4w1mw2-epsilon))&(freq_peaks<f_4w1mw2+epsilon))));
    i_2w1      = max(abs(i_peaks((freq_peaks>abs(f_2w1-epsilon))&(freq_peaks<f_2w1+epsilon))));
    i_w2       = max(abs(i_peaks((freq_peaks>abs(f_w2-epsilon))&(freq_peaks<f_w2+epsilon))));
    i_3w1      = max(abs(i_peaks((freq_peaks>abs(f_3w1-epsilon))&(freq_peaks<f_3w1+epsilon))));
    i_w1pw2    = max(abs(i_peaks((freq_peaks>abs(f_w1pw2-epsilon))&(freq_peaks<f_w1pw2+epsilon))));
    i_2w2mw1   = max(abs(i_peaks((freq_peaks>abs(f_2w2mw1-epsilon))&(freq_peaks<f_2w2mw1+epsilon))));
    i_4w1      = max(abs(i_peaks((freq_peaks>abs(f_4w1-epsilon))&(freq_peaks<f_4w1+epsilon))));
    i_2w1pw2   = max(abs(i_peaks((freq_peaks>abs(f_2w1pw2-epsilon))&(freq_peaks<f_2w1pw2+epsilon))));
    i_2w2      = max(abs(i_peaks((freq_peaks>abs(f_2w2-epsilon))&(freq_peaks<f_2w2+epsilon))));
    i_5w1      = max(abs(i_peaks((freq_peaks>abs(f_5w1-epsilon))&(freq_peaks<f_5w1+epsilon))));
    i_3w1pw2   = max(abs(i_peaks((freq_peaks>abs(f_3w1pw2-epsilon))&(freq_peaks<f_3w1pw2+epsilon))));
    i_3w2m2w1  = max(abs(i_peaks((freq_peaks>abs(f_3w2m2w1-epsilon))&(freq_peaks<f_3w2m2w1+epsilon))));
    i_w1p2w2   = max(abs(i_peaks((freq_peaks>abs(f_w1p2w2-epsilon))&(freq_peaks<f_w1p2w2+epsilon))));
    i_4w1pw2   = max(abs(i_peaks((freq_peaks>abs(f_4w1pw2-epsilon))&(freq_peaks<f_4w1pw2+epsilon))));
    i_3w2mw1   = max(abs(i_peaks((freq_peaks>abs(f_3w2mw1-epsilon))&(freq_peaks<f_3w2mw1+epsilon))));
    i_2w1p2w2  = max(abs(i_peaks((freq_peaks>abs(f_2w1p2w2-epsilon))&(freq_peaks<f_2w1p2w2+epsilon))));
    i_3w2      = max(abs(i_peaks((freq_peaks>abs(f_3w2-epsilon))&(freq_peaks<f_3w2+epsilon))));
    i_3w1p2w2  = max(abs(i_peaks((freq_peaks>abs(f_3w1p2w2-epsilon))&(freq_peaks<f_3w1p2w2+epsilon))));
    i_w1p3w2   = max(abs(i_peaks((freq_peaks>abs(f_w1p3w2-epsilon))&(freq_peaks<f_w1p3w2+epsilon))));
    i_2w1p3w2  = max(abs(i_peaks((freq_peaks>abs(f_2w1p3w2-epsilon))&(freq_peaks<f_2w1p3w2+epsilon))));
    i_4w2      = max(abs(i_peaks((freq_peaks>abs(f_4w2-epsilon))&(freq_peaks<f_4w2+epsilon))));
    i_w1p4w2   = max(abs(i_peaks((freq_peaks>abs(f_w1p4w2-epsilon))&(freq_peaks<f_w1p4w2+epsilon))));
    i_5w2      = max(abs(i_peaks((freq_peaks>abs(f_5w2-epsilon))&(freq_peaks<f_5w2+epsilon))));

    i1 = mean([i_w1 i_w2]  , 'omitnan', 'double');
    i2 = mean([i_2w1 i_2w2], 'omitnan', 'double');
    %i3 = mean([i_3w1 i_3w2], 'omitnan', 'double');
    i3 = i_3w2;
    i4 = mean([i_w1pw2, i_4w1mw2], 'omitnan', 'double');
    i5 = mean([i_w1p2w2, i_2w2mw1, i_2w1pw2, i_w2m2w1], 'omitnan', 'double');
    %i5 = mean([i_w1p2w2, i_2w2mw1, i_2w1pw2], 'omitnan', 'double');
    %i5 = mean([i_w1p2w2, i_2w2mw1], 'omitnan', 'double');
    i6 = mean([i_4w2 i_4w1], 'omitnan');
    %i7 = mean([i_w1p3w2 i_3w2mw1 i_3w1pw2], 'omitnan', 'double');
    i7 = mean([i_w1p3w2 i_3w2mw1], 'omitnan', 'double');
    
    CF2 = i4/i2;
    CF3 = i5/i3;
    %CF4 = i7/i6;
    %CF6 = i_2w1p2w2/i6;
    
    
    icorr = ((i1^2)/ (2*sqrt(8*i1*i5-3*(i4^2))));
    % Esta es la conf inicial.
    %Ba    = (i1*A) / ( i4 + sqrt(8*i1*i5-3*(i4^2)) );
    %Bc    = (i1*A) / (-i4 + sqrt(8*i1*i5-3*(i4^2)) );
    
    % Hay que establecer una lógica para escoger apropiadamente como
    % calcular los coeficientes apropiadamente y que no queden cruzados.
    Bc    = (i1*A) / ( i4 + sqrt(8*i1*i5-3*(i4^2)) );
    Ba    = (i1*A) / (-i4 + sqrt(8*i1*i5-3*(i4^2)) );
    
    harmonics = [i1, i2, i3, i4, i5];
    CF = [CF2, CF3];
    ctrnt = 8*i1*i5-3*(i4^2);
    s = echemCellModel(icorr, Ecorr, Ba, Bc, Cdl);
    
    %Cdl Estimate
%     s.response(t, E);
%     
%     dt  = diff(t);
%     dE  = diff(E);
%     
%     dE_dt = (dE./dt);
%     Ierr  = (I - s.ifarad);
%     
%     Cdl = abs(mean(Ierr(dE_dt>0 & dE_dt<inf)./dE_dt(dE_dt>0 & dE_dt<inf)));

    if Cdl_flag == 1
        % Estimate Cdl
        x0 = 1;
        fun = @(x)find_Cdl_Obj(I, s, t, E, x);
        options = optimset('Display','none');
        Cdl = fminsearch(fun,x0,options);

        if (abs(x0-Cdl)<1e-8) || isnan(Cdl) || (Cdl<0)
            Cdl = 0;
        end

        % Actualizar el modelo con Cdl
        s = echemCellModel(icorr, Ecorr, Ba, Bc, Cdl);
        irf = mean(I)-mean(s.response(t, E));

        s = echemCellModel(icorr, Ecorr, Ba, Bc, Cdl, 'rectification', irf);
    end
    
    if Cm_flag == 1
        % Pendiente del gradiente lienel de concentración.
        p_I_hat = lsr(t, (I-mean(I))./std(I), 1, 0);
        Cm = p_I_hat(1).*std(E);
        %Cm = p_I_hat(end-1).*max([Ba, Bc]);

        %Actualizar el modelo con Cm
        s = echemCellModel(icorr, Ecorr, Ba, Bc, Cdl, 'concentration', Cm);

        irf = mean(I)-mean(s.response(t, E));

        % Actualizar el modelo con irf (corriente de rectificación fadaica)
        s = echemCellModel(icorr, Ecorr, Ba, Bc, Cdl, 'concentration', Cm,...
            'rectification', irf);
    end


    %s = echemCellModel(icorr, Ecorr, Ba, Bc, Cdl);
end