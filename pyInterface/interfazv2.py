import sys  
import platform
import subprocess
import serial
import time
from PyQt5.QtCore import Qt
import pyqtgraph as pg
from pyqtgraph.Qt import QtGui, QtCore
from PyQt5.QtGui import QIcon, QPixmap
from PyQt5.QtWidgets import *
from matplotlib.backends.backend_qt5agg import FigureCanvas
from itertools import product, combinations
from matplotlib.figure import Figure
from matplotlib import *
from serial import *
import matplotlib.pyplot as plt
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import xlsxwriter
import openpyxl


class Window(QWidget):
    
    def __init__(self, parent=None):
        super(Window, self).__init__(parent)

        
        self.children = []
        self.layout1 = QVBoxLayout()
        self.layout2 = QHBoxLayout()
        self.layout3 = QHBoxLayout()
        self.layout4 = QHBoxLayout()


        self.b1 = QPushButton("LPR")
        self.b1.setCheckable(True)
        self.b1.toggle()
        self.b1.setFixedSize(200, 50)
        self.b1.clicked.connect(self.abrir1)

        self.b2 = QPushButton("EFM")
        self.b2.setCheckable(True)
        self.b2.toggle()
        self.b2.setFixedSize(200, 50)
        self.b2.clicked.connect(self.abrir2)


        self.label1 = QLabel(self)
        self.pixmap1 = QPixmap('uis.png').scaledToWidth(300)
        self.label1.setPixmap(self.pixmap1)

        self.label2 = QLabel(self)
        self.pixmap2 = QPixmap('e3t.png').scaledToWidth(200)
        self.label2.setPixmap(self.pixmap2)


        self.labelt = QLabel("TÉCNICAS ELECTROQUÍMICAS POTENCIOSTÁTICAS")

        self.labelt.setStyleSheet("color: #000000;font:30pt Arial")
        

        self.layout2.addStretch()
        self.layout2.addWidget(self.label1)
        self.layout2.addStretch()
        self.layout2.addWidget(self.label2)
        self.layout2.addStretch()

        self.layout3.addStretch()
        self.layout3.addWidget(self.labelt)
        self.layout3.addStretch()

        self.layout4.addStretch()
        self.layout4.addWidget(self.b1)
        self.layout4.addStretch()
        self.layout4.addWidget(self.b2)
        self.layout4.addStretch()

   
        self.setWindowTitle("UISTAT")
        self.layout1.addLayout(self.layout2)
        self.layout1.addLayout(self.layout3)
        self.layout1.addLayout(self.layout4)
        self.setLayout(self.layout1)
        


    def abrir1(self):
        child = ChildWindow1(name="ventana1")
        self.children.append(child)
    def abrir2(self):
        child = ChildWindow2(name="ventana2")
        self.children.append(child)

          

class ChildWindow1(QtGui.QWidget):

    def __init__(self, name=None):
        super(ChildWindow1, self).__init__()
        self.name = name
        self.layout1 = QGridLayout()
        self.layout2 = QGridLayout()
        self.layout3 = QGridLayout()

        self.setStyleSheet("background-color: white")


        self.b1 = QPushButton("Iniciar")
        self.b1.setCheckable(True)
        self.b1.toggle()
        self.b1.setFixedSize(100, 50)
        self.b1.clicked.connect(self.iniciar)
        self.b1.setStyleSheet("background-color: grey")

        self.b2 = QPushButton("Graficar")
        self.b2.setCheckable(True)
        self.b2.toggle()
        self.b2.setFixedSize(100, 50)
        self.b2.clicked.connect(self.enviar)
        self.b2.setStyleSheet("background-color: grey")


        self.l1 = QLabel("Potencial de corrosión:")
        self.l2 = QLabel("Velocidad de barrido:")
        self.l3 = QLabel("Potencial inicial:")
        self.l4 = QLabel("Potencial final:")


        self.textbox1 = QLineEdit(self)
        self.textbox2 = QLineEdit(self)
        self.textbox3 = QLineEdit(self)
        self.textbox4 = QLineEdit(self)

        self.textbox1.setFixedSize(100, 30)
        self.textbox2.setFixedSize(100, 30)
        self.textbox3.setFixedSize(100, 30)
        self.textbox4.setFixedSize(100, 30)
        
        self.textbox1.setStyleSheet("background-color: grey")
        self.textbox2.setStyleSheet("background-color: grey")
        self.textbox3.setStyleSheet("background-color: grey")
        self.textbox4.setStyleSheet("background-color: grey")
        


        self.lu1 = QLabel("mV")
        self.lu2 = QLabel("uV/s")
        self.lu3 = QLabel("mV")
        self.lu4 = QLabel("mV")



        self.win1 = pg.GraphicsWindow()
        self.p1= self.win1.addPlot(title="V vs T")
        self.curva1 = self.p1.plot(pen='y')
        self.p1.setLabel('left', "Voltaje [Vlsb]")
        self.p1.setLabel('bottom',"Tiempo [mS]")

        self.win2 = pg.GraphicsWindow()
        self.p2= self.win2.addPlot(title="i vs V")
        self.curva2 = self.p2.plot(pen='y')
        self.p2.setLabel('left', "Densidad de corriente [mA]")
        self.p2.setLabel('bottom',"Voltaje [mV]")


        self.layout1.addWidget(self.win1,0,1)
        self.layout1.addWidget(self.win2,1,1)

        self.layout2.addWidget(self.l1,0,0)
        self.layout2.addWidget(self.l2,1,0)

        self.layout3.addWidget(self.l3,0,0)
        self.layout3.addWidget(self.l4,1,0)

        self.layout2.addWidget(self.textbox1,0,1)
        self.layout2.addWidget(self.textbox2,1,1)
        
        self.layout3.addWidget(self.textbox3,0,1)
        self.layout3.addWidget(self.textbox4,1,1)
        self.layout3.addWidget(self.b1,2,1)
        self.layout3.addWidget(self.b2,3,1)

        self.layout2.addWidget(self.lu1,0,2)
        self.layout2.addWidget(self.lu2,1,2)

        self.layout3.addWidget(self.lu3,0,2)
        self.layout3.addWidget(self.lu4,1,2)


        self.layout1.addLayout(self.layout2,0,0)
        self.layout1.addLayout(self.layout3,1,0)
        self.setWindowTitle("LPR")
        self.setLayout(self.layout1)
        self.show()


        self.timer = pg.QtCore.QTimer()
        self.timer.timeout.connect(self.update)
        self.timer.start(200) 
    
    def scanrate2stepperiod(self, scanrate_uv): #este es para convertir los micro en tiempo y dar el delay de 5s
        #print('hola estoy hacienco la regla de tres')
        v_min_mv = 0.8e-3 # paso minimo del DAC
        return int(round(v_min_mv/scanrate_uv,0)*1000)

    #aca se convierte el valor que recibe en volts a bits (Vlsb)
    def voltios2valorDAC(self, volt, vref=3.3, dc=1.65):
        valorDAC = ((volt+dc)/vref)*(2**12) 
        return int(round(valorDAC, 0))

    def update(self):
     #   print("bandera")
     return None

    def iniciar(self):

        arduino = serial.Serial("COM3",9600)
        time.sleep(2)
        Vocp = float(self.textbox1.text())*1e-3
        Vinicial = Vocp + float(self.textbox3.text())*1e-3
        Vfinal = Vocp + float(self.textbox4.text())*1e-3
        delay_ms = self.scanrate2stepperiod(int(self.textbox2.text())*1e-6)
        dato = str("LPR"+" "+"0"+" "+ str(delay_ms) +" "+ str(self.voltios2valorDAC(Vinicial)) +" "+ str(self.voltios2valorDAC(Vfinal)) +"  ")
        print(dato)
        print(dato.encode())
        arduino.write(dato.encode())
        time.sleep(3)
        read_data = arduino.readline()
        print(read_data)
        read_data = arduino.readline()
        print(read_data)
        read_data = arduino.readline()
        print(read_data)
        read_data = arduino.readline()
        print(read_data)
        read_data = arduino.readline()
        print(read_data)
        #arduino.close()

    def enviar(self):
        linein=[]
        lineout1=[]
        lineout2=[]
        arduino = serial.Serial("COM3",9600)
        for i in range(0,100):
            
                line = arduino.readline().decode("utf-8",'ignore').strip('\n').strip('\r')
      
                if line.find("IN")==0:		
                    linea=line[2:len(line)]
                    try:
                        linein.append(float(linea))
                    except:
                        pass
                elif line.find("OUT1")==0:
                    linea=line[4:len(line)]
                    try:
                        lineout1.append(float(linea))
                    except:
                        pass

                elif line.find("OUT2")==0:
                    linea=line[4:len(line)]
                    try:
                        lineout2.append(float(linea))
                    except:
                        pass
        arduino.close()
        self.curva1.setData(linein)
        self.curva2.setData(lineout2,lineout1)



class ChildWindow2(QtGui.QWidget):

    def __init__(self, name=None):
        super(ChildWindow2, self).__init__()
        self.name = name
        self.layout1 = QGridLayout()
        self.layout2 = QGridLayout()
        self.layout3 = QGridLayout()
        self.setStyleSheet("background-color: white")

        self.b1 = QPushButton("Iniciar")
        self.b1.setCheckable(True)
        self.b1.toggle()
        self.b1.setFixedSize(100, 50)
        self.b1.clicked.connect(self.iniciar)
        self.b1.setStyleSheet("background-color: grey")

        self.b2 = QPushButton("Graficar")
        self.b2.setCheckable(True)
        self.b2.toggle()
        self.b2.setFixedSize(100, 50)
        self.b2.clicked.connect(self.enviar)
        self.b2.setStyleSheet("background-color: grey")

        #TEXTOS
        self.l1 = QLabel("Amplitud")
        self.l2 = QLabel("Frecuencia base")
        self.l3 = QLabel("Frecuencia 1")
        self.l4 = QLabel("Frecuencia 2")
        self.l5 = QLabel("Número de ciclos")


        self.textbox1 = QLineEdit(self)
        self.textbox2 = QLineEdit(self)
        self.textbox3 = QLineEdit(self)
        self.textbox4 = QLineEdit(self)
        self.textbox5 = QLineEdit(self)

        self.textbox1.setFixedSize(100, 30)
        self.textbox2.setFixedSize(100, 30)
        self.textbox3.setFixedSize(100, 30)
        self.textbox4.setFixedSize(100, 30)
        self.textbox5.setFixedSize(100, 30)


        self.textbox1.setStyleSheet("background-color: grey")
        self.textbox2.setStyleSheet("background-color: grey")
        self.textbox3.setStyleSheet("background-color: grey")
        self.textbox4.setStyleSheet("background-color: grey")
        self.textbox5.setStyleSheet("background-color: grey")
        

        self.lu1 = QLabel("mV")
        self.lu2 = QLabel("Hz")
        self.lu3 = QLabel("Hz")
        self.lu4 = QLabel("Hz")
        self.lu5 = QLabel()
        


        self.win1 = pg.GraphicsWindow()
        self.p1= self.win1.addPlot(title="V vs T")
        self.curva1 = self.p1.plot(pen='y')
        self.p1.setLabel('left', "Voltaje ")
        self.p1.setLabel('bottom',"Tiempo [ms]")

        self.win2 = pg.GraphicsWindow()
        self.p2= self.win2.addPlot(title="i vs V")
        self.curva2 = self.p2.plot(pen='y')
        self.p2.setLabel('left', "Densidad de corriente [mA]")
        self.p2.setLabel('bottom',"Voltaje [mV]")

        self.layout1.addWidget(self.win1,0,1)
        self.layout1.addWidget(self.win2,1,1)


        self.layout2.addWidget(self.l1,0,0)
        self.layout2.addWidget(self.l2,1,0)
        self.layout2.addWidget(self.l3,2,0)

        self.layout3.addWidget(self.l4,0,0)
        self.layout3.addWidget(self.l5,1,0)

        self.layout2.addWidget(self.textbox1,0,1)
        self.layout2.addWidget(self.textbox2,1,1)
        self.layout2.addWidget(self.textbox3,2,1)

        self.layout3.addWidget(self.textbox4,0,1)
        self.layout3.addWidget(self.textbox5,1,1)
        self.layout3.addWidget(self.b1,2,1)
        self.layout3.addWidget(self.b2,3,1)

        self.layout2.addWidget(self.lu1,0,2)
        self.layout2.addWidget(self.lu2,1,2)
        self.layout2.addWidget(self.lu3,2,2)

        self.layout3.addWidget(self.lu4,0,2)
        self.layout3.addWidget(self.lu5,1,2)

        self.layout1.addLayout(self.layout2,0,0)
        self.layout1.addLayout(self.layout3,1,0)
        self.setWindowTitle("EFM")
        self.setLayout(self.layout1)
        self.show()

        self.timer = pg.QtCore.QTimer()
        self.timer.timeout.connect(self.update)
        self.timer.start(200)

    def update(self):
     #   print("bandera")
     return None


#aca se convierte el valor que recibe en volts a bits (Vlsb)
    def voltios1valorDAC(self, volt, vref=3.3):
        valorDAC = ((volt)/vref)*(2**12) #cambiaar dc
        return int(round(valorDAC, 0))


    def iniciar(self):

        arduino = serial.Serial("COM3",9600)
        time.sleep(2)
        Amplitud1 = float(self.textbox1.text())*1e-3
        dato = str("EFM"+" "+ str(self.voltios1valorDAC(Amplitud1)) +" "+self.textbox2.text()+" "+self.textbox3.text()+" "+self.textbox4.text()+" "+self.textbox5.text()+" ")
        arduino.write(dato.encode())
        time.sleep(1)
        read_data = arduino.readline()
        print(read_data)
        read_data = arduino.readline()
        print(read_data)
        read_data = arduino.readline()
        print(read_data)
        read_data = arduino.readline()
        print(read_data)
        time.sleep(1000)
        arduino.close()

        for k in range(1000):
            self.enviar()
            time.sleep(3)

    def enviar(self):
        linein=[]
        lineout=[]
        arduino = serial.Serial("COM3",9600)

        line = arduino.readline().decode("utf-8",'ignore').strip('\n').strip('\r')
        #SENSOR1
        if line.find("IN")==0:		
            linea=line[2:len(line)]
            try:
                linein.append(float(linea))
            except:
                pass
        elif line.find("OUT1")==0:
            linea=line[4:len(line)]
            try:
                lineout.append(float(linea))
            except:
                pass
            
        elif line.find("OUT2")==0:
            linea=line[4:len(line)]
            try:
                 lineout2.append(float(linea))
            except:
                pass
            
        arduino.close()
        self.curva1.setData(linein)
        self.curva2.setData(lineout2,lineout1)     
        
            
if __name__ == '__main__':
    app = QApplication(sys.argv)

    main = Window()
    main.show()

    sys.exit(app.exec_())


