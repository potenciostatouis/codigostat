/*
  A CLI serial
  Entrada análoga, salida análoga.
  Lectura de comandos desde el puerto serial, y su ejecución.
  Lectura de un pin de entrada analógico, y asigna el resultado en un rango de 0 a 255.
  Se utiliza el resultado para configurar el PWM (modulación de ancho de pulso) de un pin de salida.
  Imprime resultados al monitor serial.
 */

// Estas constantes no cambian, son usadas para dar los nombres
// Pines escogidos:
const int analogInPin  = A0;  // Pin de entrada analógico a cual esta conectado el potenciometro
const int analogOutPin = PB_1;  // Pin de salida analógica al cual esta conectado el LED

const uint8_t EFM_wave[101] = { 
  0xFE, 0xFA, 0xF0, 0xDF, 0xCA, 0xB2, 0x9A, 0x82,
  0x6E, 0x5E, 0x53, 0x4F, 0x50, 0x56, 0x5F, 0x6B,
  0x78, 0x82, 0x8A, 0x8D, 0x8B, 0x84, 0x77, 0x67,
  0x54, 0x40, 0x2C, 0x1C, 0x11, 0x0B, 0x0C, 0x14,
  0x23, 0x38, 0x50, 0x6B, 0x87, 0xA0, 0xB6, 0xC7, 
  0xD2, 0xD6, 0xD4, 0xCD, 0xC1, 0xB2, 0xA3, 0x95,
  0x89, 0x82, 0x7F, 0x82, 0x89, 0x95, 0xA3, 0xB2, 
  0xC1, 0xCD, 0xD4, 0xD6, 0xD2, 0xC7, 0xB6, 0xA0, 
  0x87, 0x6B, 0x50, 0x38, 0x23, 0x14, 0x0C, 0x0B,
  0x11, 0x1C, 0x2C, 0x3F, 0x54, 0x67, 0x77, 0x84, 
  0x8B, 0x8D, 0x8A, 0x82, 0x78, 0x6B, 0x5F, 0x56,
  0x50, 0x4F, 0x53, 0x5E, 0x6E, 0x82, 0x9A, 0xB2,
  0xCA, 0xDF, 0xF0, 0xFA, 0xFE  
};

const uint8_t  sine_wave[256] = {
  0x80, 0x83, 0x86, 0x89, 0x8C, 0x90, 0x93, 0x96,
  0x99, 0x9C, 0x9F, 0xA2, 0xA5, 0xA8, 0xAB, 0xAE,
  0xB1, 0xB3, 0xB6, 0xB9, 0xBC, 0xBF, 0xC1, 0xC4,
  0xC7, 0xC9, 0xCC, 0xCE, 0xD1, 0xD3, 0xD5, 0xD8,
  0xDA, 0xDC, 0xDE, 0xE0, 0xE2, 0xE4, 0xE6, 0xE8,
  0xEA, 0xEB, 0xED, 0xEF, 0xF0, 0xF1, 0xF3, 0xF4,
  0xF5, 0xF6, 0xF8, 0xF9, 0xFA, 0xFA, 0xFB, 0xFC,
  0xFD, 0xFD, 0xFE, 0xFE, 0xFE, 0xFF, 0xFF, 0xFF,
  0xFF, 0xFF, 0xFF, 0xFF, 0xFE, 0xFE, 0xFE, 0xFD,
  0xFD, 0xFC, 0xFB, 0xFA, 0xFA, 0xF9, 0xF8, 0xF6,
  0xF5, 0xF4, 0xF3, 0xF1, 0xF0, 0xEF, 0xED, 0xEB,
  0xEA, 0xE8, 0xE6, 0xE4, 0xE2, 0xE0, 0xDE, 0xDC,
  0xDA, 0xD8, 0xD5, 0xD3, 0xD1, 0xCE, 0xCC, 0xC9,
  0xC7, 0xC4, 0xC1, 0xBF, 0xBC, 0xB9, 0xB6, 0xB3,
  0xB1, 0xAE, 0xAB, 0xA8, 0xA5, 0xA2, 0x9F, 0x9C,
  0x99, 0x96, 0x93, 0x90, 0x8C, 0x89, 0x86, 0x83,
  0x80, 0x7D, 0x7A, 0x77, 0x74, 0x70, 0x6D, 0x6A,
  0x67, 0x64, 0x61, 0x5E, 0x5B, 0x58, 0x55, 0x52,
  0x4F, 0x4D, 0x4A, 0x47, 0x44, 0x41, 0x3F, 0x3C,
  0x39, 0x37, 0x34, 0x32, 0x2F, 0x2D, 0x2B, 0x28,
  0x26, 0x24, 0x22, 0x20, 0x1E, 0x1C, 0x1A, 0x18,
  0x16, 0x15, 0x13, 0x11, 0x10, 0x0F, 0x0D, 0x0C,
  0x0B, 0x0A, 0x08, 0x07, 0x06, 0x06, 0x05, 0x04,
  0x03, 0x03, 0x02, 0x02, 0x02, 0x01, 0x01, 0x01,
  0x01, 0x01, 0x01, 0x01, 0x02, 0x02, 0x02, 0x03,
  0x03, 0x04, 0x05, 0x06, 0x06, 0x07, 0x08, 0x0A,
  0x0B, 0x0C, 0x0D, 0x0F, 0x10, 0x11, 0x13, 0x15,
  0x16, 0x18, 0x1A, 0x1C, 0x1E, 0x20, 0x22, 0x24,
  0x26, 0x28, 0x2B, 0x2D, 0x2F, 0x32, 0x34, 0x37,
  0x39, 0x3C, 0x3F, 0x41, 0x44, 0x47, 0x4A, 0x4D,
  0x4F, 0x52, 0x55, 0x58, 0x5B, 0x5E, 0x61, 0x64,
  0x67, 0x6A, 0x6D, 0x70, 0x74, 0x77, 0x7A, 0x7D
};


int sensorValue = 0;        // Valor leído desde el potenciómetro
int outputValue = 0;        // Valor de salida al PWM (analog out)

#define MAX_CHAR_LEN 64
#define PROMPT "uisstat> "
#define CLI_TIMEOUT 60000

#define LED RED_LED

void setup() {
  // Configura el LED Rojo, como pin de salida
  pinMode(LED, OUTPUT);
  
  // initialize serial communications at 9600 bps:
  Serial.begin(9600); 
  delay(100);
}

void loop() {

  do_console();
//  delay(1000);                     
}

//==================================
// Comando LPR
//==================================
/*
 * LPR : Se genera un PWM cambiando el ancho desde un #[0 a 255](Inicio) y un #[0 a 255](Final) cada #segundos
 * Argumentos:
 * #1 -> Ancho inicial
 * #2 -> Ancho Final
 * #3 -> Tiempo entre cambio
 * #4 -> el paso de cambio (por defecto 1)
 */

void do_lpr(char* args)
{
  //definición de variables 
  char *arg;                 // Aquí guardo el argumento actual
  arg = get_token(&args);    // Con esta función saco los argumentos
  long num = 0;              // Aquí voy a guardar el argumento como tipo número entero grande (atol)
  int numi = 0;              // Aquí voy a guardar el argumento como tipo número entero pequeño(atoi)
  double numf = 0.0;         // Aquí voy a guardar el argumento como tipo número decimal (atof)
 // int i=0;
  int rep=0;
  int error = 0;

  int startNum = 0;  // Ancho inicial
  int endNum   = 0;  // Ancho Final
  int delay_ms = 0;  // tiempo entre cambio
  int step     = 1;  // el paso de cambio (por defecto 1)


  //Lógica del comando
  if (strlen(arg)<=0)
  {
    error = 1;
  }
  else
  {
    startNum = atoi(arg);
    
    arg = get_token(&args);   //los argumentos númericos
    if (strlen(arg)>0)
    {
      endNum = atoi(arg);
    }
    else
    {
      error = 1;
    }

    arg = get_token(&args);   //los argumentos númericos
    if (strlen(arg)>0)
    {
      delay_ms = atoi(arg);
    }
    else
    {
      error = 1;
    }

    arg = get_token(&args);   //los argumentos númericos
    if (strlen(arg)>0)
    {
      step = atoi(arg);
    }
  }

  // Ejecución del comando

    Serial.print("\n Ancho Inicial: ");
    Serial.println(startNum);

    Serial.print("Ancho Final: ");
    Serial.println(endNum);

    Serial.print("Tiempo entre cambio: ");
    Serial.println(delay_ms);

    Serial.print("Tiempo entre cambio (paso) : ");
    Serial.println(step);

  if(startNum>endNum)
  {
    error=1;
    Serial.println("\n El valor inicial es mayor que el valor final");
  }


  if(endNum>255 || startNum>255 || step>255)
  {
    error=1;
    Serial.println("\n Los valores deben ser menores a 255");
  }


  if(error == 1)
  {
    Serial.println("\n LPR : Genera un pwm cambiando el ancho desde un #[0 a 255] inicio y un #[0 a 255] final cada #segundos");
    Serial.println("Se esperan 3 argumentos númericos");
  }else
  {
    
    for(int i=startNum;i<endNum;i=i+step)
    {
      
      analogWrite(analogOutPin, i); //i recorre los valores de la matriz
      delay(delay_ms);
      Serial.println(i); //timewait: y leer los datos de la tabla
    }
  }
 
}
//
////==================================
//// Señal seno
////==================================
/*
 * SIN : Se genera una señal seno de amplitud A y frecuencia F y número de ciclos N
 * Argumentos:
 * #1 -> Amplitud
 * #2 -> Frecuenca
 * #3 -> Número de ciclos
 */

void do_sin(char* args)
{
  char *arg;                 // Aquí guardo el argumento actual
  arg = get_token(&args);    // Con esta función saco los argumentos
  int amp = 0;  // Amplitud
  int freq   = 0;  // Frecuencia
  int ncyc = 0;  // Número de ciclos
  int error = 0;
  float delay_ms = 0;
  
  Serial.println("holi\n");
  
  //Lógica del comando
  if (strlen(arg)<=0)
  {
    error = 1;
  }
  else
  {
    amp = atoi(arg);
    
    arg = get_token(&args);   //los argumentos númericos
    if (strlen(arg)>0)  //se pregunta si el argumento tiene caracterees
    {
      freq = atoi(arg);
    }
    else
    {
      error = 1;
    }

    arg = get_token(&args);   //los argumentos númericos
    if (strlen(arg)>0)
    {
      ncyc = atoi(arg);
    }
    else
    {
      error = 1;
    }
  }

  // Ejecución del comando

    Serial.print("\n Amplitud [V]: ");
    Serial.println(amp);

    Serial.print("Frecuencia [mHz]: ");
    Serial.println(freq);

    Serial.print("Número de ciclos: ");
    Serial.println(ncyc);

    delay_ms = ((1/(freq/1000.0))/256)*1000;

     Serial.print("Delay entre puntos de la tabla: ");
    Serial.println(delay_ms);

// aca viene la max frecuencia y la max amplitud
  // para amp debe encontrarse el valor max de amplitud en volts,
  // para freq , la freq min y max en Hz, el # de ciclos se deja normal.
  
  if(amp>255 || freq>6000 || ncyc>255) // temporalmente se limita por el valor max del pwm
  {
    error=1;
    Serial.println("\n Los valores deben ser menores a 255");
  }


  if(error == 1)
  {
    Serial.println("\n SIN : Genera una señal seno de amplitud A y la frecuenca F y número de ciclos N");
    Serial.println("Se esperan 3 argumentos númericos");
  }else
  {
     
//el for
    for(int i=0;i<256;i++)
    {
     
      analogWrite(analogOutPin, sine_wave[i]); //i recorre los valores de la matriz sine_wave
      delay(ceil(delay_ms));
      Serial.println(sine_wave[i]);  //timewait: y leer los datos de la tabla
    }
  }
}

//
////==================================
//// Señal EFM
////==================================
/*
 * EFM : Se genera una señal seno de amplitud A y frecuencia F y número de ciclos N
 * Argumentos:
 * #1 -> Amplitud
 * #2 -> Frecuencia
 * #3 -> Número de ciclos
 */

void do_EFM(char* args)
{
  char *arg;                 // Aquí guardo el argumento actual
  arg = get_token(&args);    // Con esta función saco los argumentos
  int amp = 0;  // Amplitud
  int freq   = 0;  // Frecuencia
  int ncyc = 0;  // Número de ciclos
  int error = 0;
  float delay_ms = 0;
  
 // Serial.println("holi\n");
  
  //Lógica del comando
  if (strlen(arg)<=0)
  {
    error = 1;
  }
  else
  {
    amp = atoi(arg);
    
    arg = get_token(&args);   //los argumentos númericos
    if (strlen(arg)>0)  //se pregunta si el argumento tiene caracterees
    {
      freq = atoi(arg);
    }
    else
    {
      error = 1;
    }

    arg = get_token(&args);   //los argumentos númericos
    if (strlen(arg)>0)
    {
      ncyc = atoi(arg);
    }
    else
    {
      error = 1;
    }
  }

  // Ejecución del comando

    Serial.print("\n Amplitud [V]: ");
    Serial.println(amp);

    Serial.print("Frecuencia [mHz]: ");
    Serial.println(freq);

    Serial.print("Número de ciclos: ");
    Serial.println(ncyc);

    delay_ms = ((1/(freq/1000.0))/101)*1000;

     Serial.print("Delay entre puntos de la tabla: ");
    Serial.println(delay_ms);

// aca viene la max frecuencia y la max amplitud
  // para amp debe encontrarse el valor max de amplitud en volts,
  // para freq , la freq min y max en Hz, el # de ciclos se deja normal.
  
  if(amp>100 || freq>6000 || ncyc>100) // temporalmente se limita por el valor max del pwm
  {
    error=1;
    Serial.println("\n Los valores deben ser menores a 255");
  }


  if(error == 1)
  {
    Serial.println("\n EFM : Genera una sumatoria de cosenos de amplitud A y la frecuencia F y número de ciclos N");
    Serial.println("Se esperan 3 argumentos númericos");
  }else
  {
     
//el for
    for(int i=0;i<100;i++)
    {
     
      analogWrite(analogOutPin, EFM_wave[i]); //i recorre los valores de la matriz sine_wave
      delay(ceil(delay_ms));
      Serial.println(EFM_wave[i]);  //timewait: y leer los datos de la tabla
    }
  }
}



//==================================
// LED Comando de ejemplo
//==================================
void do_led(char* args)
{
  char *arg;  
  arg = get_token(&args);
  long num = 0;
  char line [21];
  int i=0;
  int delay_ms=0;
  int rep=0;

  if (strlen(arg)<=0)
  {
    if(digitalRead(LED)==HIGH)
    {
      digitalWrite(LED, LOW);    // turn the LED off by making the voltage LOW  
    }
    else
    {
      digitalWrite(LED, HIGH);   // turn the LED on (HIGH is the voltage level)  
    }
  }
  else
  {
    if(strcmp(arg, "on"  ) == 0)
    {
      digitalWrite(LED, HIGH);   // turn the LED on (HIGH is the voltage level)
    }

    if(strcmp(arg, "off"  ) == 0)
    {
      digitalWrite(LED, LOW);    // turn the LED off by making the voltage LOW
    }

    if(strcmp(arg, "blink"  ) == 0) //LPR
    {
    
      arg = get_token(&args);   //los argumentos numericos
      delay_ms = atoi(arg);

      arg = get_token(&args);
      rep = atoi(arg);

      for(i=0;i<rep;i++)
      {
        digitalWrite(LED, HIGH);   // turn the LED on (HIGH is the voltage level)
        delay(delay_ms);
        digitalWrite(LED, LOW);    // turn the LED off by making the voltage LOW
        delay(delay_ms);
      }
    }

    
  }
}

//==================================
// Una sola Medición analógica
//==================================
void do_analogRead()
{
  // read the analog in value:
  sensorValue = analogRead(analogInPin);            
  // map it to the range of the analog out:
  outputValue = map(sensorValue, 0, 1023, 0, 255);  
  // change the analog out value:
  analogWrite(analogOutPin, outputValue);           

  // print the results to the serial monitor:
  Serial.print("\nsensor = " );                       
  Serial.print(sensorValue);      
  Serial.print("\t output = ");      
  Serial.println(outputValue);   

  // wait 10 milliseconds before the next loop
  // for the analog-to-digital converter to settle
  // after the last reading:
  delay(10);
}


void do_console()
{
  //*** CONSOLA ***
  char buffer[MAX_CHAR_LEN];
  int buf_count=0;
  int k=0;
  long time_ms=0;
  long difftime_ms = 0;
  char c;
  char *buf;

  buf = buffer;

  Serial.print("Esperando Comandos por serial:\n");
  time_ms = millis();
  difftime_ms = millis() - time_ms;
  Serial.print(PROMPT);
  while(difftime_ms < 60000)
  {
    if(Serial.available()>3)
    {
      for(k=0;k<MAX_CHAR_LEN;k++)
      {
        buf[k]=NULL;
      }
  
      time_ms = millis(); 
      buf_count=0;
      c=0;
      delay(50);
      c = Serial.read();
      while(c!=-1)
      {
        if (c>0 && c != '\n' && c != '\r' && Serial.available())
        {
          //PD("\nc:%c - count:%d", c, buf_count);
          buf[buf_count] = c;
          buf_count++;
        }
        else
        {
          break;
        }
        
        c = Serial.read();  
      }
  
      if (millis() < time_ms)
      {
        time_ms = millis();
      }
      
      if ((unsigned)strlen(buf)>0)
      {
        Serial.print(buf);
        do_command(buf);
        Serial.print(PROMPT);
      }
      
    }
    difftime_ms = millis() - time_ms;
  }
}

//=============================================================================
//        PARCE DO NOT TOUCH
//=============================================================================
static char *get_token(char **str)
{
  char *c, *d;

  c = (char *)strchr(*str, ' ');
  if(c == NULL) {
    d = *str;
    *str = *str+strlen(*str);
    return d;
  }
  *c = 0;
  d = *str;
  *str = c+1;
  return d;
}

int do_command(char *c)
{
  char *token;
  char xcmd = 1;

  token = get_token(&c);

  if     (strcmp(token, "help"     ) == 0) do_help();
  else if(strcmp(token, "aread"    ) == 0) do_analogRead();
  else if(strcmp(token, "led"      ) == 0) do_led(c);
  else if(strcmp(token, "LPR"   ) == 0) do_lpr(c);
  else if(strcmp(token, "SENO"   ) == 0) do_sin(c);
  else if(strcmp(token, "EFM"   ) == 0) do_EFM(c);
  else if(strcmp(token, ""         ) != 0)
  {
    Serial.println("\n");  
    Serial.println(token);
    Serial.println(" ");
    Serial.println("No se encontró el comando\n\r");
    xcmd = 0;
  }
  return xcmd;
}

//============================
//    HELP
//============================
void do_help()
{
  Serial.println("\n Hola soy la ayuda\n");
}
