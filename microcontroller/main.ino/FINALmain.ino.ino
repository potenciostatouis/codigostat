// A CLI serial
//Entrada análoga, salida análoga.
//Lectura de comandos desde el puerto serial, y su ejecución.
//Lectura de un pin de entrada analógico, y asigna el resultado en un rango de 0 a 255.


// Estas constantes no cambian, son usadas para dar los nombres
// Pines escogidos:
const int analogInPin  = A0;  // Pin de entrada analógico a cual esta conectado el sensor
const int analogOutPin = A8;  // Pin de salida analógica al cual esta conectado el filtro
float a;
float b;

// Matriz de datos que construyen la señal EFM
long EFM_wave[1000] = {
  0xFFD, 0xFFB, 0xFF8, 0xFF4, 0xFEF, 0xFE8, 0xFE1, 0xFD8, 0xFCE, 0xFC3, 0xFB7, 
  0xFAA, 0xF9C, 0xF8C, 0xF7C, 0xF6A, 0xF58, 0xF44, 0xF30, 0xF1A, 0xF03, 0xEEC, 
  0xED3, 0xEBA, 0xEA0, 0xE85, 0xE69, 0xE4C, 0xE2E, 0xE10, 0xDF1, 0xDD1, 0xDB1, 
  0xD8F, 0xD6E, 0xD4B, 0xD28, 0xD05, 0xCE1, 0xCBC, 0xC97, 0xC72, 0xC4C, 0xC26, 
  0xBFF, 0xBD9, 0xBB1, 0xB8A, 0xB63, 0xB3B, 0xB13, 0xAEC, 0xAC4, 0xA9C, 0xA74, 
  0xA4C, 0xA24, 0x9FC, 0x9D5, 0x9AD, 0x986, 0x95F, 0x938, 0x912, 0x8EB, 0x8C6, 
  0x8A0, 0x87B, 0x856, 0x832, 0x80F, 0x7EB, 0x7C9, 0x7A7, 0x785, 0x765, 0x744, 
  0x725, 0x706, 0x6E8, 0x6CB, 0x6AE, 0x692, 0x677, 0x65D, 0x643, 0x62B, 0x613, 
  0x5FC, 0x5E6, 0x5D1, 0x5BD, 0x5AA, 0x597, 0x586, 0x575, 0x566, 0x557, 0x549, 
  0x53D, 0x531, 0x526, 0x51C, 0x513, 0x50C, 0x505, 0x4FF, 0x4FA, 0x4F5, 0x4F2, 
  0x4F0, 0x4EF, 0x4EE, 0x4EF, 0x4F0, 0x4F2, 0x4F6, 0x4FA, 0x4FE, 0x504, 0x50B, 
  0x512, 0x51A, 0x523, 0x52C, 0x536, 0x541, 0x54D, 0x559, 0x566, 0x573, 0x581, 
  0x590, 0x59F, 0x5AF, 0x5BF, 0x5D0, 0x5E1, 0x5F2, 0x604, 0x616, 0x628, 0x63B, 
  0x64E, 0x661, 0x674, 0x688, 0x69C, 0x6AF, 0x6C3, 0x6D7, 0x6EB, 0x6FF, 0x713, 
  0x727, 0x73B, 0x74E, 0x762, 0x775, 0x788, 0x79B, 0x7AD, 0x7C0, 0x7D2, 0x7E3, 
  0x7F4, 0x805, 0x815, 0x825, 0x835, 0x844, 0x852, 0x860, 0x86D, 0x87A, 0x886, 
  0x891, 0x89C, 0x8A6, 0x8AF, 0x8B8, 0x8C0, 0x8C7, 0x8CD, 0x8D3, 0x8D8, 0x8DC, 
  0x8DF, 0x8E1, 0x8E3, 0x8E3, 0x8E3, 0x8E2, 0x8E0, 0x8DE, 0x8DA, 0x8D6, 0x8D0, 
  0x8CA, 0x8C3, 0x8BB, 0x8B2, 0x8A8, 0x89E, 0x892, 0x886, 0x879, 0x86B, 0x85C, 
  0x84C, 0x83B, 0x82A, 0x818, 0x805, 0x7F1, 0x7DD, 0x7C8, 0x7B2, 0x79B, 0x784, 
  0x76C, 0x753, 0x73A, 0x720, 0x706, 0x6EB, 0x6CF, 0x6B3, 0x697, 0x67A, 0x65D, 
  0x63F, 0x621, 0x602, 0x5E3, 0x5C4, 0x5A4, 0x585, 0x565, 0x545, 0x524, 0x504, 
  0x4E4, 0x4C3, 0x4A3, 0x482, 0x461, 0x441, 0x421, 0x401, 0x3E0, 0x3C1, 0x3A1, 
  0x382, 0x362, 0x344, 0x325, 0x307, 0x2EA, 0x2CC, 0x2B0, 0x294, 0x278, 0x25D, 
  0x242, 0x228, 0x20F, 0x1F6, 0x1DF, 0x1C7, 0x1B1, 0x19B, 0x186, 0x172, 0x15F, 
  0x14D, 0x13B, 0x12B, 0x11B, 0x10D, 0x0FF, 0x0F2, 0x0E7, 0x0DC, 0x0D2, 0x0CA, 
  0x0C2, 0x0BC, 0x0B6, 0x0B2, 0x0AF, 0x0AD, 0x0AC, 0x0AC, 0x0AD, 0x0B0, 0x0B3, 
  0x0B8, 0x0BE, 0x0C5, 0x0CD, 0x0D6, 0x0E1, 0x0EC, 0x0F9, 0x107, 0x116, 0x126, 
  0x137, 0x149, 0x15C, 0x170, 0x186, 0x19C, 0x1B4, 0x1CC, 0x1E6, 0x200, 0x21B, 
  0x238, 0x255, 0x273, 0x292, 0x2B2, 0x2D3, 0x2F4, 0x317, 0x33A, 0x35E, 0x382,
  0x3A7, 0x3CD, 0x3F4, 0x41B, 0x442, 0x46A, 0x493, 0x4BC, 0x4E6, 0x510, 0x53A,
  0x565, 0x590, 0x5BB, 0x5E7, 0x613, 0x63F, 0x66B, 0x697, 0x6C3, 0x6F0, 0x71C,
  0x749, 0x775, 0x7A1, 0x7CD, 0x7F9, 0x825, 0x851, 0x87C, 0x8A7, 0x8D2, 0x8FC,
  0x926, 0x950, 0x979, 0x9A2, 0x9CA, 0x9F1, 0xA19, 0xA3F, 0xA65, 0xA8A, 0xAAF,
  0xAD3, 0xAF6, 0xB19, 0xB3A, 0xB5B, 0xB7B, 0xB9B, 0xBB9, 0xBD7, 0xBF3, 0xC0F,
  0xC2A, 0xC44, 0xC5D, 0xC75, 0xC8C, 0xCA2, 0xCB7, 0xCCB, 0xCDE, 0xCF0, 0xD01,
  0xD11, 0xD20, 0xD2E, 0xD3B, 0xD46, 0xD51, 0xD5B, 0xD63, 0xD6A, 0xD71, 0xD76,
  0xD7B, 0xD7E, 0xD80, 0xD81, 0xD81, 0xD81, 0xD7F, 0xD7C, 0xD78, 0xD73, 0xD6E,
  0xD67, 0xD5F, 0xD57, 0xD4D, 0xD43, 0xD38, 0xD2C, 0xD1F, 0xD12, 0xD04, 0xCF5,
  0xCE5, 0xCD4, 0xCC3, 0xCB2, 0xC9F, 0xC8C, 0xC79, 0xC65, 0xC50, 0xC3B, 0xC25,
  0xC0F, 0xBF9, 0xBE2, 0xBCB, 0xBB4, 0xB9C, 0xB84, 0xB6C, 0xB54, 0xB3B, 0xB22,
  0xB0A, 0xAF1, 0xAD8, 0xABF, 0xAA6, 0xA8E, 0xA75, 0xA5C, 0xA44, 0xA2C, 0xA13,
  0x9FC, 0x9E4, 0x9CD, 0x9B6, 0x99F, 0x989, 0x973, 0x95D, 0x948, 0x934, 0x920,
  0x90C, 0x8F9, 0x8E7, 0x8D5, 0x8C3, 0x8B3, 0x8A3, 0x893, 0x885, 0x877, 0x869,
  0x85D, 0x851, 0x846, 0x83C, 0x832, 0x82A, 0x822, 0x81A, 0x814, 0x80F, 0x80A,
  0x806, 0x803, 0x801, 0x800, 0x7FF, 0x800, 0x801, 0x803, 0x806, 0x80A, 0x80F,
  0x814, 0x81A, 0x822, 0x82A, 0x832, 0x83C, 0x846, 0x851, 0x85D, 0x869, 0x877,
  0x885, 0x893, 0x8A3, 0x8B3, 0x8C3, 0x8D5, 0x8E7, 0x8F9, 0x90C, 0x920, 0x934,
  0x948, 0x95D, 0x973, 0x989, 0x99F, 0x9B6, 0x9CD, 0x9E4, 0x9FC, 0xA13, 0xA2C,
  0xA44, 0xA5C, 0xA75, 0xA8E, 0xAA6, 0xABF, 0xAD8, 0xAF1, 0xB0A, 0xB22, 0xB3B,
  0xB54, 0xB6C, 0xB84, 0xB9C, 0xBB4, 0xBCB, 0xBE2, 0xBF9, 0xC0F, 0xC25, 0xC3B,
  0xC50, 0xC65, 0xC79, 0xC8C, 0xC9F, 0xCB2, 0xCC3, 0xCD4, 0xCE5, 0xCF5, 0xD04,
  0xD12, 0xD1F, 0xD2C, 0xD38, 0xD43, 0xD4D, 0xD57, 0xD5F, 0xD67, 0xD6E, 0xD73,
  0xD78, 0xD7C, 0xD7F, 0xD81, 0xD81, 0xD81, 0xD80, 0xD7E, 0xD7B, 0xD76, 0xD71,
  0xD6A, 0xD63, 0xD5B, 0xD51, 0xD46, 0xD3B, 0xD2E, 0xD20, 0xD11, 0xD01, 0xCF0,
  0xCDE, 0xCCB, 0xCB7, 0xCA2, 0xC8C, 0xC75, 0xC5D, 0xC44, 0xC2A, 0xC0F, 0xBF3,
  0xBD7, 0xBB9, 0xB9B, 0xB7B, 0xB5B, 0xB3A, 0xB19, 0xAF6, 0xAD3, 0xAAF, 0xA8A,
  0xA65, 0xA3F, 0xA19, 0x9F1, 0x9CA, 0x9A2, 0x979, 0x950, 0x926, 0x8FC, 0x8D2,
  0x8A7, 0x87C, 0x851, 0x825, 0x7F9, 0x7CD, 0x7A1, 0x775, 0x749, 0x71C, 0x6F0,
  0x6C3, 0x697, 0x66B, 0x63F, 0x613, 0x5E7, 0x5BB, 0x590, 0x565, 0x53A, 0x510,
  0x4E6, 0x4BC, 0x493, 0x46A, 0x442, 0x41B, 0x3F4, 0x3CD, 0x3A7, 0x382, 0x35E,
  0x33A, 0x317, 0x2F4, 0x2D3, 0x2B2, 0x292, 0x273, 0x255, 0x238, 0x21B, 0x200,
  0x1E6, 0x1CC, 0x1B4, 0x19C, 0x186, 0x170, 0x15C, 0x149, 0x137, 0x126, 0x116,
  0x107, 0x0F9, 0x0EC, 0x0E1, 0x0D6, 0x0CD, 0x0C5, 0x0BE, 0x0B8, 0x0B3, 0x0B0,
  0x0AD, 0x0AC, 0x0AC, 0x0AD, 0x0AF, 0x0B2, 0x0B6, 0x0BC, 0x0C2, 0x0CA, 0x0D2,
  0x0DC, 0x0E7, 0x0F2, 0x0FF, 0x10D, 0x11B, 0x12B, 0x13B, 0x14D, 0x15F, 0x172,
  0x186, 0x19B, 0x1B1, 0x1C7, 0x1DF, 0x1F6, 0x20F, 0x228, 0x242, 0x25D, 0x278,
  0x294, 0x2B0, 0x2CC, 0x2EA, 0x307, 0x325, 0x344, 0x362, 0x382, 0x3A1, 0x3C1,
  0x3E0, 0x400, 0x421, 0x441, 0x461, 0x482, 0x4A3, 0x4C3, 0x4E4, 0x504, 0x524,
  0x545, 0x565, 0x585, 0x5A4, 0x5C4, 0x5E3, 0x602, 0x621, 0x63F, 0x65D, 0x67A,
  0x697, 0x6B3, 0x6CF, 0x6EB, 0x706, 0x720, 0x73A, 0x753, 0x76C, 0x784, 0x79B,
  0x7B2, 0x7C8, 0x7DD, 0x7F1, 0x805, 0x818, 0x82A, 0x83B, 0x84C, 0x85C, 0x86B,
  0x879, 0x886, 0x892, 0x89E, 0x8A8, 0x8B2, 0x8BB, 0x8C3, 0x8CA, 0x8D0, 0x8D6,
  0x8DA, 0x8DE, 0x8E0, 0x8E2, 0x8E3, 0x8E3, 0x8E3, 0x8E1, 0x8DF, 0x8DC, 0x8D8,
  0x8D3, 0x8CD, 0x8C7, 0x8C0, 0x8B8, 0x8AF, 0x8A6, 0x89C, 0x891, 0x886, 0x87A,
  0x86D, 0x860, 0x852, 0x844, 0x835, 0x825, 0x815, 0x805, 0x7F4, 0x7E3, 0x7D2,
  0x7C0, 0x7AD, 0x79B, 0x788, 0x775, 0x762, 0x74E, 0x73B, 0x727, 0x713, 0x6FF,
  0x6EB, 0x6D7, 0x6C3, 0x6AF, 0x69C, 0x688, 0x674, 0x661, 0x64E, 0x63B, 0x628,
  0x616, 0x604, 0x5F2, 0x5E1, 0x5D0, 0x5BF, 0x5AF, 0x59F, 0x590, 0x581, 0x573,
  0x566, 0x559, 0x54D, 0x541, 0x536, 0x52C, 0x523, 0x51A, 0x512, 0x50B, 0x504,
  0x4FE, 0x4FA, 0x4F6, 0x4F2, 0x4F0, 0x4EF, 0x4EE, 0x4EF, 0x4F0, 0x4F2, 0x4F5,
  0x4FA, 0x4FF, 0x505, 0x50C, 0x513, 0x51C, 0x526, 0x531, 0x53D, 0x549, 0x557,
  0x566, 0x575, 0x586, 0x597, 0x5AA, 0x5BD, 0x5D1, 0x5E6, 0x5FC, 0x613, 0x62B,
  0x643, 0x65D, 0x677, 0x692, 0x6AE, 0x6CB, 0x6E8, 0x706, 0x725, 0x744, 0x765,
  0x785, 0x7A7, 0x7C9, 0x7EB, 0x80F, 0x832, 0x856, 0x87B, 0x8A0, 0x8C6, 0x8EB,
  0x912, 0x938, 0x95F, 0x986, 0x9AD, 0x9D5, 0x9FC, 0xA24, 0xA4C, 0xA74, 0xA9C,
  0xAC4, 0xAEC, 0xB13, 0xB3B, 0xB63, 0xB8A, 0xBB1, 0xBD9, 0xBFF, 0xC26, 0xC4C,
  0xC72, 0xC97, 0xCBC, 0xCE1, 0xD05, 0xD28, 0xD4B, 0xD6E, 0xD8F, 0xDB1, 0xDD1,
  0xDF1, 0xE10, 0xE2E, 0xE4C, 0xE69, 0xE85, 0xEA0, 0xEBA, 0xED3, 0xEEC, 0xF03,
  0xF1A, 0xF30, 0xF44, 0xF58, 0xF6A, 0xF7C, 0xF8C, 0xF9C, 0xFAA, 0xFB7, 0xFC3,
  0xFCE, 0xFD8, 0xFE1, 0xFE8, 0xFEF, 0xFF4, 0xFF8, 0xFFB, 0xFFD, 0xFFD
};

int sensorValue = 0;        // Valor leído desde el sensor
int outputValue = 0;        // Valor de salida al PWM (analog out)

#define MAX_CHAR_LEN 64
#define PROMPT "uisstat> "
#define CLI_TIMEOUT 60000
//DAC 12 BITS
#include <Wire.h>
#include <math.h>

//The MCP4725 from Adafruit:
//  I2C address is 0x62 ( by default A0 has a 10k pull-down resistor ).
//  I2C address is 0x63 if A0 is connected to VCC.
#define MCP4725_ADDR 0x62           //I2c address..

int ch[ 1000 ] , cl[ 1000 ];
int ch_lpr , cl_lpr;
void setup() {

  Wire.begin();           // SCL(3) and SDA(3) by default in Tiva C.
  Wire.setModule(2);      // Changing to SCL(0)and SDA(0),(PB_2 and PB_3).
  // initialize serial communications at 9600 bps:
  Serial.begin(9600);
  delay(100);
}

void loop() {
  do_console();
  //  delay(1000);
}

//==================================
// Comando LPR
//==================================
/*
   LPR : Se genera un PWM cambiando el ancho desde un #[0 a 255](Inicio) y un #[0 a 255](Final) cada #segundos
   Argumentos:
   #1 -> Ancho inicial
   #2 -> Ancho Final
   #3 -> Tiempo entre cambio
   #4 -> el paso de cambio (por defecto 1)
*/

void do_lpr(char* args)
{
  //definición de variables
  char *arg;                 // Aquí guardo el argumento actual
  arg = get_token(&args);    // Con esta función saco los argumentos
  long num = 0;              // Aquí voy a guardar el argumento como tipo número entero grande (atol)
  int numi = 0;              // Aquí voy a guardar el argumento como tipo número entero pequeño(atoi)
  double numf = 0.0;         // Aquí voy a guardar el argumento como tipo número decimal (atof)
  int rep = 0;
  int error = 0;

  int Potc = 0;   // potencial de corrosión (0mV para las celdas dummy)
  float delay_ms = 0.0;  // tiempo entre cambio
  int startNum = 0;  // Ancho inicial (pot inicial)
  int endNum   = 0;  // Ancho Final (pot final)
  int step     = 1;  // el paso de cambio (por defecto 1)

  //Lógica del comando
  if (strlen(arg) <= 0)
  {
    error = 1;
  }
  else
  {
    Potc = atoi(arg);

    arg = get_token(&args);   //los argumentos númericos
    if (strlen(arg) > 0)
    {
      delay_ms = atof(arg);
    }
    else
    {
      error = 1;
    }

    arg = get_token(&args);   //los argumentos númericos
    if (strlen(arg) > 0)
    {
      startNum = atoi(arg);
    }
    else
    {
      error = 1;
    }

    arg = get_token(&args);   //los argumentos númericos
    if (strlen(arg) > 0)
    {
      endNum = atoi(arg);
    }
    else
    {
      error = 1;
    }

    arg = get_token(&args);   //los argumentos númericos
    if (strlen(arg) > 0)
    {
      step = atoi(arg);
    }
  }

  // Ejecución del comando

//  Serial.print("\nPotencial de Corrosion: ");
//  Serial.println(Potc);
//  Serial.print("Tiempo entre cambio: ");
// Serial.println(delay_ms);
//  Serial.print("Ancho Inicial (Potencial Inicial): ");
//  Serial.println(startNum);
// Serial.print("Ancho Final (Potencial Final): ");
//  Serial.println(endNum);
//  Serial.print("Tiempo entre cambio (paso) : ");
//  Serial.println(step);


  if (startNum > endNum) // condición de los límites
  {
    error = 1;
    Serial.println("\n El valor inicial es mayor que el valor final");
  }

  //  if(endNum>255 || startNum>255 || step>255)
  //  {
  //    error=1;
  //    Serial.println("\n Los valores deben ser menores a 255");
  //  }

  if (error == 1)
  {
    Serial.println("\n LPR : Genera un pwm cambiando el ancho desde un #[0 a 255] inicio y un #[0 a 255] final cada #segundos");
    Serial.println("Se esperan 4 argumentos númericos");
  } else
  {

    for (int i = startNum; i < endNum; i = i + step)
    {
     // for (int i = 0; i < 1024; i++) // Acá inicia la lectura del DAC
    //  {
        ch_lpr = int( i / 256 );     // the 4 most significant bits.
        cl_lpr = i - ch_lpr * 256;   // the 8 least significant bits.
        Wire.beginTransmission(MCP4725_ADDR);
        //Fast Mode Write Command.
        Wire.write( ch_lpr );        // the 4 most significant bits.
        Wire.write( cl_lpr );        // the 8 least significant bits.
        Wire.endTransmission(); // end tranmission
        delay(int(delay_ms*(4/5)));

        
        a = analogRead(A7);
        b = analogRead(A6);
        Serial.print("IN");
        Serial.println(i);    //valor de la tensiòn
        Serial.print("OUT1");
        Serial.println(a); //valor de la corriente
        Serial.print("OUT2");
        Serial.println(b); //valor de la corriente        
        delay(int(delay_ms/5));

        
        //      analogWrite(analogOutPin, i); //i recorre los valores de la matriz
        //      delay(delay_ms);
        //      Serial.println(i); //timewait: y leer los datos de la tabla
     // }
    }
  }
}


////==================================
//// Señal EFM
////==================================
/*
   EFM : Se genera una señal seno de amplitud A y frecuencia F y número de ciclos N
   Argumentos:
   #1 -> Amplitud
   #2 -> Frecuencia Base
   #3 -> Frecuencia 1
   #4 -> Frecuencia 2
   #5 -> Número de ciclos
*/

void do_EFM(char* args)
{
  char *arg;                 // Aquí guardo el argumento actual
  arg = get_token(&args);    // Con esta función saco los argumentos
  int amp = 1;  // Amplitud
  int freq   = 1;  // Frecuencia Base que es 0,1
  int freq1 = 2; // Frecuencia 1
  int freq2 = 5; // Frecuencia 2
  int ncyc = 0;  // Número de ciclos
  int error = 0;
  float delay_ms = 0;

  //Lógica del comando
  if (strlen(arg) <= 0)
  {
    error = 1;
  }
  else
  {
    amp = atoi(arg);

    arg = get_token(&args);   //los argumentos númericos
    if (strlen(arg) > 0) //se pregunta si el argumento tiene caracteres
    {
      freq = atoi(arg); // frecuencia base (0,1)
    }
    else
    {
      error = 1;
    }

    arg = get_token(&args);   //los argumentos númericos
    if (strlen(arg) > 0) //se pregunta si el argumento tiene caracteres
    {
      freq1 = atoi(arg);  // frecuencia 1
    }
    else
    {
      error = 1;
    }

    arg = get_token(&args);   //los argumentos númericos
    if (strlen(arg) > 0) //se pregunta si el argumento tiene caracterees
    {
      freq2 = atoi(arg); // frecuencia 2
    }
    else
    {
      error = 1;
    }

    arg = get_token(&args);   //los argumentos númericos
    if (strlen(arg) > 0)
    {
      ncyc = atoi(arg); // nùmero de ciclos
    }
    else
    {
      error = 1;
    }
  }

  // Ejecución del comando

//  Serial.print("\n Amplitud [V]: ");
//  Serial.println(amp);
//  Serial.print("Frecuencia Base [mHz]: ");
//  Serial.println(freq);
//  Serial.print("Frecuencia 1 [mHz]: ");
//  Serial.println(freq1);
//  Serial.print("Frecuencia 2 [mHz]: ");
//  Serial.println(freq2);
//  Serial.print("Número de ciclos: ");
//  Serial.println(ncyc);
  //    delay_ms = ((1/(freq/1000.0))/101)*1000;
  delay_ms = 10;
//  Serial.print("Delay entre puntos de la tabla: ");
//  Serial.println(delay_ms);

  //
  if (amp > 100 || freq > 6000 || ncyc > 100) // temporalmente se limita por el valor max del pwm
  {
    error = 1;
    Serial.println("\n Los valores deben ser menores a 255");
  }

  if (error == 1)
  {
    Serial.println("\n EFM : Genera una sumatoria de cosenos de amplitud A y la frecuencia base F, Frecuencia 1 y 2 para cada coseno y número de ciclos N");
    Serial.println("Se esperan 5 argumentos númericos");
  } else
  {

    //el for
    for (int i = 0; i < ncyc; i++)
    {
      //ESTO ES LO DEL DAC
      for (int i = 0; i < 1000; i++)
      {
        ch[ i ] = int( EFM_wave[i] / 256 );     // the 4 most significant bits.
        cl[ i ] = EFM_wave[i] - ch[ i ] * 256;   // the 8 least significant bits.
        Wire.beginTransmission(MCP4725_ADDR);
        //Fast Mode Write Command.
        Wire.write( ch[ i ] );        // the 4 most significant bits.
        Wire.write( cl[ i ] );        // the 8 least significant bits.       // the 4 most significant bits.
        Wire.endTransmission(); // end tranmission
        delay(ceil(delay_ms));
        
        delay(int(delay_ms*(4/5)));
        
        
        a = analogRead(A7);
        b = analogRead(A6);   
        Serial.print("IN");
        Serial.println(EFM_wave[i]);    //valor de la tensiòn
        Serial.print("OUT1");
        Serial.println(a); //valor de la corriente
        Serial.print("OUT2");
        Serial.println(b); //valor de la corriente        
      
        
        delay(int(delay_ms/5));

        
      }
      analogWrite(analogOutPin, EFM_wave[i]); //i recorre los valores de la matriz sine_wave
            delay(ceil(delay_ms));
      Serial.println(EFM_wave[i]);  //timewait: y leer los datos de la tabla
    }
  }
}


//==================================
// Una sola Medición analógica (deben ser tres mediciones)
//==================================
void do_analogRead()
{
  // read the analog in value:
  sensorValue = analogRead(analogInPin);
  // map it to the range of the analog out:
  outputValue = map(sensorValue, 0, 1023, 0, 255);
  // change the analog out value:
  analogWrite(analogOutPin, outputValue);

  // print the results to the serial monitor:
  Serial.print("\nsensor = " );
  Serial.print(sensorValue);
  Serial.print("\t output = ");
  Serial.println(outputValue);

  // wait 10 milliseconds before the next loop
  // for the analog-to-digital converter to settle
  // after the last reading:
  delay(10);
}

//void do_analogWrite(char* args)
//{
//  char *arg;
//  arg = get_token(&args);
//  long num = 0;
//  char line [21];
//  int i=0;
//  int delay_ms=0;
//  int Estart = 0;
//  int Eend =0;
//  int outputValue = 0;
//
//  if (strlen(arg)<=0)
//  {
//    analogWrite(analogOutPin, 128);
//  }
//  else
//  {
//    Estart = atoi(arg);
//    arg = get_token(&args);
//    Eend = atoi(arg);
//    arg = get_token(&args);
//    delay_ms = atoi(arg);
//
//    outputValue = Estart;
//    analogWrite(analogOutPin, outputValue);
//    delay(delay_ms);
//    while(outputValue<Eend)
//    {
//      outputValue = outputValue +1;
//      analogWrite(analogOutPin, outputValue);
//      delay(delay_ms);
//    }
//  }
//
//}

void do_console()
{
  //*** CONSOLA ***
  char buffer[MAX_CHAR_LEN];
  int buf_count = 0;
  int k = 0;
  long time_ms = 0;
  long difftime_ms = 0;
  char c;
  char *buf;

  buf = buffer;

  Serial.print("Esperando Comandos por serial:\n");
  time_ms = millis();
  difftime_ms = millis() - time_ms;
  Serial.print(PROMPT);
  while (difftime_ms < 60000)
  {
    if (Serial.available() > 3)
    {
      for (k = 0; k < MAX_CHAR_LEN; k++)
      {
        buf[k] = NULL;
      }

      time_ms = millis();
      buf_count = 0;
      c = 0;
      delay(50);
      c = Serial.read();
      while (c != -1)
      {
        if (c > 0 && c != '\n' && c != '\r' && Serial.available())
        {
          //PD("\nc:%c - count:%d", c, buf_count);
          buf[buf_count] = c;
          buf_count++;
        }
        else
        {
          break;
        }

        c = Serial.read();
      }

      if (millis() < time_ms)
      {
        time_ms = millis();
      }

      if ((unsigned)strlen(buf) > 0)
      {
        Serial.print(buf);
        do_command(buf);
        Serial.print(PROMPT);
      }

    }
    difftime_ms = millis() - time_ms;
  }
}

//=============================================================================
//        PARCE DO NOT TOUCH
//=============================================================================
static char *get_token(char **str)
{
  char *c, *d;

  c = (char *)strchr(*str, ' ');
  if (c == NULL) {
    d = *str;
    *str = *str + strlen(*str);
    return d;
  }
  *c = 0;
  d = *str;
  *str = c + 1;
  return d;
}

int do_command(char *c)
{
  char *token;
  char xcmd = 1;

  token = get_token(&c);

  if     (strcmp(token, "help"     ) == 0) do_help();
  else if (strcmp(token, "aread"    ) == 0) do_analogRead();
  else if (strcmp(token, "LPR"   ) == 0) do_lpr(c);
  else if (strcmp(token, "EFM"   ) == 0) do_EFM(c);
  else if (strcmp(token, ""         ) != 0)
  {
    Serial.println("\n");
    Serial.println(token);
    Serial.println(" ");
    Serial.println("No se encontró el comando\n\r");
    xcmd = 0;
  }
  return xcmd;
}

//============================
//    HELP
//============================
void do_help()
{
  Serial.println("\n Hola soy la ayuda, puedes reiniciar la técnica y volver a comenzar\n");
}
