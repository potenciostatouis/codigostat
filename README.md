﻿# uistat

Programación del potenciostato en la plataforma TIVA
El siguiente software fue desarrollado para la aplicación de técnicas electroquimicas generadas desde el microcontrolador para medir la velocidad de corrosión en celdas dummy.

Se adjunta archivo board para su implementación.

Instrucciones de uso:
1. Conecte los electrodos de acuerdo a las etiquetas correspondientes:
(WE: Electrodo de trabajo, CE: Electrodo de control, RE: Electrodo de referencia).
2. Energice la tarjeta con el cargador de 9[V]-2[A] en el puerto jack y conecte la TIVA por su puerto micro USB a la computadora para establecer la comunicación.
3. Se inicializa la interfaz y se selecciona la prueba a realizar haciendo clic en el botón LPR para "Resistencia a la polarización lineal" o en el botón EFM para "Modulación electroquímica de frecuencia".
4. Cada técnica solicita los datos para su ejecución, ingrese estos datos correspondientes a la prueba e inicialicelas al hacer clic en el botón iniciar.
5. Dé clic en el botón enviar y espere un lapso de tiempo para que la prueba termine, y ésta genera automáticamente un archivo tipo xlxs que contiene los datos en mV/mA para su posterior análisis de velocidad de corrosión.

Nota: se adjunta los códigos en matlab que calculan la velocidad de corrosión para LPR y para EFM.